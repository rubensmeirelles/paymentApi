﻿using Microsoft.VisualBasic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.Models;

[Table("Pedidos")]
public class Pedido
{

    public Pedido()
    {
        PedidosItens = new Collection<PedidoItem>(); 
    }
    [Key]
    public int PedidoId { get; set; }

    public DateTime? DataPedido { get; set; }

    public int? VendedorId { get; set; }
    public string Status { get; set; } = "Aguardando pagamento";
    public ICollection<PedidoItem>? PedidosItens { get; set; }
}
