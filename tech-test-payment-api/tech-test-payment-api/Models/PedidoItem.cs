﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.Models;

[Table("PedidosItens")]
public class PedidoItem
{
    [Key]
    public int PedidoItemId { get; set; }

    [Required]
    [StringLength(80)]
    public string? Item { get; set; }

    public int PedidoId { get; set; }

    [JsonIgnore]
    public Pedido? Pedido { get; set; }
}

