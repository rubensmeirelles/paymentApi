﻿using tech_test_payment_api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    [Route("[controller]")]
    [ApiController]

    
    public class PedidosController : ControllerBase
    {
        private readonly Context.AppDbContext _context;

        public PedidosController(AppDbContext context)
        {
            _context = context;
        }

        //[HttpGet("pedidos/{id:int}")]
        //public ActionResult<IEnumerable<Pedido>> GetPedidosItens()
        //{
        //   return _context.Pedidos.Include(p => p.PedidosItens).ToList();
        //}

        [HttpGet]
        public ActionResult<IEnumerable<Pedido>> Get()
        {
            var pedidos = _context.Pedidos.AsNoTracking().ToList();

            if(pedidos is null)
            {
                try
                {
                    return NotFound("Nenhum pedido cadastrado.");
                }
                catch (Exception)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Ocorreu um erro inesperado.");
                }
                
            }
            return pedidos;
        }

        [HttpGet("{id:int}", Name = "ObterPedidoPorId")]
        public ActionResult<IEnumerable<Pedido>> Get(int id)
        {
            //var pedido = _context.Pedidos.FirstOrDefault(c => c.PedidoId == id);
            var pedido = _context.Pedidos.Include(p => p.PedidosItens).FirstOrDefault(c => c.PedidoId == id); 
            if (pedido == null)
            {
                try
                {
                    return NotFound("Pedido não encontrado.");
                }
                catch (Exception)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Ocorreu um erro inesperado.");
                }
            }
            return Ok(pedido);
        }

        [HttpPost]
        public ActionResult Post(Pedido pedido)
        {
            if (pedido is null)
                return BadRequest();

            _context.Pedidos.Add(pedido);
            _context.SaveChanges();
            return new CreatedAtRouteResult("ObterPedido", new { id = pedido.PedidoId }, pedido);
        }

        [HttpPut("{id:int}")]
        public ActionResult Put(int id, Pedido pedido)
        {
            var pedidoBanco = _context.Pedidos.Find(id);

            if (id != pedido.PedidoId)
            {
                return BadRequest();
            }

            if (Validador.PodeAtualizar(pedido, pedidoBanco))
            {
                pedidoBanco.Status = pedido.Status;
                _context.SaveChanges();
                return Ok(pedidoBanco);
            }
            if (pedidoBanco.Status == "Enviado para Transportadora") return UnprocessableEntity("Pedido encaminhado à transportadora, cancelamento não permitido");
            if (pedidoBanco.Status == "Entregue") return UnprocessableEntity("A venda já foi entregue");
            if (pedidoBanco.Status == "Cancelada") return UnprocessableEntity("A venda foi cancelada");
            return UnprocessableEntity("Status não permitido");

            //_context.Entry(pedido).State = EntityState.Modified;
            //_context.SaveChanges();

            //return Ok(pedido);
        }


        public static class Validador
        {
           public static bool PodeAtualizar(Pedido pedido, Pedido pedidoBanco)
            {
                if (pedidoBanco.Status.Equals("Aguardando Pagamento"))
                {
                    if (pedido.Status.Equals("Pagamento Aprovado") || pedido.Status.Equals("Cancelada"))
                    {
                        return true;
                    }
                }
                if (pedidoBanco.Status.Equals("Pagamento Aprovado"))
                {
                    if (pedido.Status.Equals("Enviado para Transportadora") || pedido.Status.Equals("Cancelada"))
                    {
                        return true;
                    }
                }
                if (pedidoBanco.Status.Equals("Enviado para Transportadora"))
                {
                    if (pedido.Status.Equals("Entregue"))
                    {
                        return true;
                    }
                }
                return false;
            }
        }


        //[HttpDelete("{id:int}")]
        //public ActionResult Delete(int id)
        //{
        //    var pedido = _context.Pedidos.FirstOrDefault(p => p.PedidoId == id);

        //    if (pedido is null)
        //    {
        //        return NotFound("Pedido não encontrada!");
        //    }
        //    _context.Pedidos.Remove(pedido);
        //    _context.SaveChanges();

        //    return Ok(pedido);
        //}
    }
}
