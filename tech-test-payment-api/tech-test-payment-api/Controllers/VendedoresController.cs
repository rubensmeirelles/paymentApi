﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendedoresController : ControllerBase
    {
        private readonly AppDbContext _context;

        public VendedoresController(AppDbContext context)
        {
            _context = context;
        }
        [HttpPost(Name = "CriarVendedor")]
        public ActionResult Post(Vendedor vendedor)
        {
            if (vendedor is null)
                return BadRequest();

            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();
            return new CreatedAtRouteResult("CriarVendedor", new { id = vendedor.VendedorId }, vendedor);
        }

    }
}
