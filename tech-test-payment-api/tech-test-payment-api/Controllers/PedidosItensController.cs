﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace ApiCatalogo.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PedidosItensController : ControllerBase
    {
        private readonly AppDbContext _context;

        public PedidosItensController(AppDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<PedidoItem>> Get()
        {
            var pedidoItems = _context.PedidosItens.ToList();
            if(pedidoItems is null)
            {
                try
                {
                    return NotFound("Nenhum item cadastrado.");
                }
                catch (Exception)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Ocorreu um erro inesperado.");
                }                
            }
            return pedidoItems;
        }

        //[HttpGet("{id:int}", Name = "ObterPedidoItem")]
        //public ActionResult<PedidoItem> Get(int id)
        //{
        //    var pedidoItem = _context.PedidosItens.FirstOrDefault(p => p.PedidoItemId == id);
        //    if(pedidoItem is null)
        //    {
        //        try
        //        {
        //            return NotFound("Item não encontrado.");
        //        }
        //        catch (Exception)
        //        {
        //            return StatusCode(StatusCodes.Status500InternalServerError, "Ocorreu um erro inesperado.");
        //        }                
        //    }
        //    return pedidoItem;
        //}

        [HttpPost(Name = "ObterPedido")]
        public ActionResult Post(PedidoItem pedidoItem)
        {
            if (pedidoItem is null)
                return BadRequest();

            _context.PedidosItens.Add(pedidoItem);
            _context.SaveChanges();
            return new CreatedAtRouteResult("ObterPedido", new { id = pedidoItem.PedidoItemId }, pedidoItem);
        }

        //[HttpPut("{id:int}")]
        //public ActionResult Put(int id, PedidoItem pedidoItem) 
        //{
        //    if(id != pedidoItem.PedidoItemId)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(pedidoItem).State = EntityState.Modified;
        //    _context.SaveChanges();

        //    return Ok(pedidoItem);
        //}

        //[HttpDelete("{id:int}")]
        //public ActionResult Delete(int id)
        //{
        //    var pedidoItem = _context.PedidosItens.FirstOrDefault(p => p.PedidoItemId == id);

        //    if(pedidoItem is null)
        //    {
        //        return NotFound("Produto não encontrado!");
        //    }
        //    _context.PedidosItens.Remove(pedidoItem);
        //    _context.SaveChanges();

        //    return Ok(pedidoItem);
        //}
    }
}
